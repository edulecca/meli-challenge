**Melli Challenge BE**

Package contains a full stack framework, a backend (:3001) and frontend (:3000) servers

1) git clone git@gitlab.com:edulecca/meli-challenge.git
2) cd meli-challenge  (**challenge is the dev branch**)
3) npm start (will start the service as dev environments on both envs, FED and BE)

**A) Frondend View,** could be place to translate
*  input morse translate to letters
*  input bits translate to morse
*  input letter translate to morse

*use as examples*
- bits> 000000001101101100111000001111110001111110011111100000001110111111110111011100000001100011111100000111111001111110000000110000110111111110111011100000011011100000000000
- morse> .... --- .-.. .- -- . .-.. ..
- letters> hello world

B) Backend service, got a REST API which could be acces on these 3 endpoints
*  /api/Bits2Human > input bits translate to letters
*  /api/Bits2Morse > input bits translate to morse
*  /api/2Morse2Letters > input morse or letters transtlte to letters or morse

*use as examples*
- curl -X POST -H "Content-Type:application/json" http://localhost:3000/api/Bits2Human -d '{"text":"000000001101101100111000001111110001111110011111100000001110111111110111011100000001100011111100000111111001111110000000110000110111111110111011100000011011100000000000"}'
- curl -X POST -H "Content-Type:application/json" http://localhost:3000/api/2Morse2Letters -d '{"text":". ... - .- -- --- ...  .-.. .. ... - ---"}'
- curl -X POST -H "Content-Type:application/json" http://localhost:3000/api/2Morse2Letters -d '{"text":"hola mundo"}'
