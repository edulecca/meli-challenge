import TranslatorSingleton from './TranslatorSingleton';

class TranslatorImplementation {
    constructor(data) {
      this._singleton = new TranslatorSingleton(data);
    }

    /** TRANSLATE
    * Bits => Morse
    */ // @return {string}
    decodeBits2morse(){
        return this._singleton._decodedMorseToArray().map(e => e.join(' ')).join(' '.repeat(4));
    }

    /** TRANSLATE
    * string Morse => string Letters
    * string Letter => string Morse
    */ //@return {string}
    translate2Human(){
        let arrayParsed = (!this._singleton._hasLetter()) ? this._singleton._decodedCharsToArray(' ') : this._singleton._decodedCharsToArray();
        return this._singleton._abcMatcher(arrayParsed).map(e => e.join('')).join(' ');
    }

    /** Morse parced as Array => string Letters
    * string Letter => string Morse
    */ //@return {string}
    bits2Human(){
        let morseArrayParsed = this._singleton._decodedMorseToArray();
        return this._singleton._abcMatcher(morseArrayParsed).map(e => e.join('')).join(' ');
    }
    
}
export default TranslatorImplementation;