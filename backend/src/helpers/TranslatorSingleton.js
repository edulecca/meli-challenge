import abcJSON from '../public/abc-morse.json';

class TranslatorSingleton {
    constructor(data) {
      this._data = data;
    }
    
    set data(data) {
      this._data = data;
    }

    get data() {
      return this._data;
    }

    _isBit(){
        let flag = false;
        this._data.split('').some(e => { 
            if (!e.match(/^[0-1]+$/)) 
                flag = true;
        });
        return !flag;
    }
    
    _hasLetter(){
        let flag = false;
        this._data.split('').some(e => { 
            if (e.match(/^[a-zA-Z]+$/) != null) 
                flag = true;
        });
        return flag;
    }

    /**
     * Iterate the BITs and parse into an Array as Blocks per
     * @return {array} Each change between 1 or 0 will an array value
     */
    _parsePulses(){
        let codeMorseAsArray = [], j =0, counter = 0;
        for (var i = 0; i < this._data.length; i++){
            codeMorseAsArray[j] = [this._data[i] , ++counter ]; 
            (parseInt(this._data[i]) != parseInt(this._data[i+1])) ? (counter=0, j++) : '';
        }
        return codeMorseAsArray;
    }

    /**
     * Infer the lenght of the pulse
     * @return {object} Avarage of Pulse and Space
     */
    _bitsData(){
        let sumOnes = 0, sumZeros = 0, qtyZerosBlocks = 0, qtyOnesBlocks = 0, largestOnes = 0, largestZeros = 0;
        this._parsePulses().forEach( value => {
            switch(parseInt(value[0])) {
                case 0:
                    sumZeros = sumZeros+value[1];
                    ++qtyZerosBlocks;
                    largestZeros = (largestZeros >= value[1]) ? largestZeros : value[1];
                    break;
                case 1: 
                    sumOnes = sumOnes+value[1];
                    ++qtyOnesBlocks;
                    largestOnes = (largestOnes >= value[1]) ? largestOnes : value[1];
                    break;
            };
        });
        return { 
            averagePulseInfer: Math.round(largestOnes/(sumOnes/qtyOnesBlocks)),
            averageSpacesInfer: Math.round(largestZeros/(sumZeros/qtyZerosBlocks))
        };
    }

    /**
     * Get MorseCode of a given bits
     * @param {char} space - use to be delimiter on the array to help format
     * @param {char} longSpace - use to be delimiter on the array to help format
     * @return {array} Place on each postion of the array what intends to be 
     * the words and sentences, depending on th large of the space pulse
     */
    _getMorseCode(space = ' ', longSpace = ' '.repeat(4)){
        let pulse = this._bitsData().averagePulseInfer;
        let spacesAvarage = this._bitsData().averageSpacesInfer;
        function setChar(e) {
            let dot = '.', dash = '-', offset = Math.floor(e[1]/pulse), char = '';
            if (!parseInt(e[0])){
                char = (e[1]/2 >= spacesAvarage) ? longSpace : space;
                char = ( offset <= 1 ) ? '' : char;
                return char;
            } else {
                return (offset == 1) ? dot.repeat(offset) : dash;
            }
        };
        return this._parsePulses().map(e => setChar(e));
    }

    /**
     * Tranform MorseCode to Array to operate on it
     * '_' & '>' are helpers chars to simplify decode
     * '_' indent to be simple letter space, meanwhile '>' is a large pause
     * @return {array} Place on each postion of the array what intends to be a simbol
     */
    _decodedMorseToArray() {
        let morseSplitSentences = this._getMorseCode('_', '>').join('').split('>'), sentences = [], j = 0;
        morseSplitSentences.forEach(e => (e.length) ? sentences[j++] = e.split('_') : '' );
        return sentences;
    }

    /**
     * Transform a string characters to array to be manipulated
     * @param {char} offset - use to be delimiter on the array to help format
     * @return {array}  Array of Chars formated to lowercase
     */
    _decodedCharsToArray(offset = ''){
        var array = [], aux = [], i = 0;
        this._data.split(offset).forEach(e => {
            aux[0] = e.toLowerCase();
            array[i++] = aux;
            aux = [];
        });
        return array;
    }
    
    /** Perform the match between letters and morse symbols
    *  @param {array} Array parsedMorse or parsed Letters
    */ //@return {array} parsed Morse or parsed Letters
    _abcMatcher(arrayParsed){
        const alphabet = abcJSON;
        const switcher = this._translatorSwitcher();
        let i = 0, j = 0, text = [], chars = [];
        arrayParsed.forEach(sentences => {
            sentences.forEach(letter => {
                alphabet.forEach(sign => {
                    (letter === sign[switcher.sign2Translate]) ? chars[j++] = sign[switcher.sign2Look] : '';
                });
            });
            text[i++] = chars;
            j=0;
            chars = [];
        });
        return text;
    }

    /**
     * will detect what kind of input it is
     */// @return {object}  
    _translatorSwitcher(){
        const checker = this._hasLetter();
        return {
            sign2Translate: checker ? 'letter' : 'morse',
            sign2Look: checker ? 'morse' : 'letter'
        }
    }
}

export default TranslatorSingleton;