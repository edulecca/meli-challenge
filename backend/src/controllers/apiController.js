//import TranslatorSingleton from '../helpers/TranslatorSingleton';
import TranslatorImplementation from '../helpers/TranslatorImplementation';


exports.bitsToHuman = (req, res) => {
    try {
        const classHelper = new TranslatorImplementation(req.body.text);
        const message = {
            code: 200,
            response: classHelper.bits2Human()
        };
        return res.send(message);
    } catch {
        const message = {
            code: 400,
            response: 'Server Error'
        };
        return res.send(message);
    }
};

exports.decodeBitsTomorse = (req, res) => {
    try {
        const classHelper = new TranslatorImplementation(req.body.text);
            const message = {
                code: 200,
                response: classHelper.decodeBits2morse(),
            };
            return res.send(message);
        
    } catch {
        const message = {
            code: 400,
            response: 'Server Error'
        };
        return res.send(message);
    }
};

exports.toMorseToLetters = (req, res) => {
    try {
        const classHelper = new TranslatorImplementation(req.body.text);
            const message = {
                code: 200,
                response: classHelper.translate2Human(),
            };
            return res.send(message);
        
    } catch {
        const message = {
            code: 400,
            response: 'Server Error'
        };
        return res.send(message);
    }
};

exports.errorHandler = (err, req, res) => {
    // const message = {
    //     code: 400,
    //     response: err.message,
    // };
    // return res.send(message);
};