import 'dotenv/config';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import routes from './routes';
const path = require('path');
const app = express();
app.use(cors());
app.use(express.urlencoded());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//ADD VIRTUAL STATIC FOLDER TO SERVE JS FOR TESTING
app.use('/static', express.static(path.join(__dirname+'/public')));
app.use('/api', routes.api);

app.listen(process.env.PORT, () =>
  console.log(`Example app listening on port ${process.env.PORT}!`),
);