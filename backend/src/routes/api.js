import { Router } from 'express';
const router = Router();
import apiController from '../controllers/apiController';

router.post('/Bits2Human', apiController.bitsToHuman, apiController.errorHandler);
router.post('/Bits2Morse', apiController.decodeBitsTomorse, apiController.errorHandler);
router.post('/2Morse2Letters', apiController.toMorseToLetters, apiController.errorHandler);

export default router;