import React from 'react';
import { isBit } from '../helpers/functions';

class FormMorse extends React.Component {
    formInput = React.createRef();

    determinePath = value => {
        const paths = {
            bits2morse: '/api/Bits2Morse',
            bits2letters: '/api/Bits2Human',
            toMorseToLetters: '/api/2Morse2Letters',
        };
        // TODO NOT REQUIRED GO TO bit2letters
        var url = isBit(value) ? paths.bits2morse : paths.toMorseToLetters;
        return url;
    }

    transformInput = event => {
        event.preventDefault();
        let inputToDecode = this.formInput.current.value;
        const goAPI = this.determinePath(inputToDecode);
        fetch(goAPI, {
            method: "POST",
            body: JSON.stringify({
                text: inputToDecode
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => {
            return response.json()
        }).then(data => {
            this.props.addSearch({ inputToDecode, ...data });
        });
        this.formInput.current.value = '';
    };

    render() {
        return (
            <form onSubmit={this.transformInput} className="form-decode">
                <input 
                    className="input-encode"
                    type="text"
                    name="toencode"
                    ref={this.formInput}
                    autoComplete="off" />
                <button type="submit">Submit</button>
            </form>
        );
    }
}

export default FormMorse
