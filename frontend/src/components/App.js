import React from "react";
import FormMorse from "./FormMorse";
import Displayer from "./Displayer";
import Header from './Header';
let counter = 0;

class App extends React.Component {
  state = {
    searches: {},
  };

  addSearch = search => {
    const searches = {...this.state.searches};
    searches[counter++] = search;
    this.setState({ searches });
  };

  render() {
    return (
      <div>
        <Header mobileImage="/static/logo__small.png" desktopImage="/static/logo__large_plus.png" />
        <FormMorse addSearch={this.addSearch}/>
        <ul className="old-searches">
          {Object.keys(this.state.searches).map(key => (
            <Displayer key={key} data={this.state.searches[key]}/>
          ))}
        </ul> 
      </div>

    )
  }
}
 
export default App;