import React from 'react';

class Header extends React.Component {
    render() {
        return (
            <header className="top">
                <img src={this.props.mobileImage} alt="mobileImage" />
                <img src={this.props.desktopImage} alt="desktopImage"/>
            </header>
        );
    }
}

export default Header