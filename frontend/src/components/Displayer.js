import React from 'react';


class Displayer extends React.Component {
    render() {
        const { inputToDecode, response } = this.props.data;
        return (
            <li className="displayer-container">
                <h2>{response}</h2>
                <h4>{inputToDecode}</h4>
            </li>
        );
    }
}

export default Displayer
