import 'dotenv/config';
import React from 'react';
import { render } from 'react-dom';
import App from './components/App';
import './styles.css';
//import Router from {""./components/Router"};

render(<App />, document.getElementById('main'));
