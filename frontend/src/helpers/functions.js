export function isBit(value){
    let flag = false;
    // eslint-disable-next-line
    value.split('').some(e => { 
        if (!e.match(/^[0-1]+$/)) 
            flag = true;
    });
    return !flag;
}

export function hasLetter(value){
    let flag = false;
    // eslint-disable-next-line
    this.data.split('').some(e => { 
        if (e.match(/^[a-zA-Z]+$/) != null) 
            flag = true;
    })
    return flag;
}